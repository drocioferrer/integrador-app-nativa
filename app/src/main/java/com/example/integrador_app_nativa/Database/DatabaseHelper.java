package com.example.integrador_app_nativa.Database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "BD";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_CREATE =
            "CREATE TABLE carrito (_id integer primary key autoincrement, "
                    + "idArticulo text, descripcion text, precio int, cantidad int, subtotal int, imagen text);";

    private SQLiteDatabase bd;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(DATABASE_CREATE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS carrito");
        onCreate(db);
    }

    public SQLiteDatabase open() {
        bd = this.getWritableDatabase();
        return bd;
    }

    public void close() {
        bd.close();
    }
}