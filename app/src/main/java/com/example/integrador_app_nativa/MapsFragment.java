package com.example.integrador_app_nativa;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MapsFragment extends Fragment {

    private MarkerOptions marker;

    FirebaseDatabase database;
    DatabaseReference myRef;

    String fecha;

    public MapsFragment (String fecha) {
        this.fecha = fecha;
    }

    private OnMapReadyCallback callback = new OnMapReadyCallback() {

        @Override
        public void onMapReady(GoogleMap googleMap) {

            // MARCADORES DE PEDIDOS
            myRef.child("Pedido").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for(DataSnapshot ds : dataSnapshot.getChildren()) {

                        if (ds.child("fecha").getValue(String.class).equals(fecha)) {
                            String [] parts = (ds.child("ubicacion").getValue(String.class)).split(",");
                            String lat = parts[0];
                            String lng = parts[1];
                            String numero = String.valueOf(ds.child("numero").getValue(Long.class));
                            LatLng punto = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
                            googleMap.addMarker(new MarkerOptions().position(punto).title("Pedido N° " + numero));
                            googleMap.moveCamera(CameraUpdateFactory.newLatLng(punto));
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            // MARCADOR PUNTO ORIGEN
            LatLng home = new LatLng(-34.7751811,-58.2701577);
            marker = new MarkerOptions();
            marker.position(home);
            marker.title("Nosotros");
            marker.icon(BitmapDescriptorFactory.fromResource(android.R.drawable.star_on));
            googleMap.addMarker(marker);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(home, 15));

        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        return inflater.inflate(R.layout.fragment_maps, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(callback);
        }

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
    }
}