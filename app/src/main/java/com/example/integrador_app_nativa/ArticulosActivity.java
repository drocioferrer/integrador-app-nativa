package com.example.integrador_app_nativa;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.integrador_app_nativa.Articulos.AdapterArticuloAdmin;
import com.example.integrador_app_nativa.Articulos.AgregarArticuloActivity;
import com.example.integrador_app_nativa.Articulos.ArticuloPedido;
import com.example.integrador_app_nativa.modeloCliente.Articulo;
import com.example.integrador_app_nativa.modeloCliente.ArticuloAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class ArticulosActivity extends AppCompatActivity {
    ArticuloPedido articuloPedidoselec= new ArticuloPedido();
    Button agregarArt;
    DatabaseReference myRef ;
    FirebaseDatabase database;
    ArrayList<ArticuloPedido> listaArticulos;
    ListView rcArticulos;
    private AdapterArticuloAdmin adap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articulos);

        agregarArt = findViewById(R.id.agregarArticuloButton);
        rcArticulos= findViewById(R.id.artadmin);
        database= FirebaseDatabase.getInstance();
        myRef= database.getReference();
        listaArticulos= new ArrayList<>();
        adap= new AdapterArticuloAdmin(this,listaArticulos);
        rcArticulos.setAdapter(adap);
        agregarArt.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                startActivity(new Intent(ArticulosActivity.this, AgregarArticuloActivity.class));
            }
        });

        rcArticulos.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView adapterView, View view, int i, long l) {
                        final int posicion = i;
                        articuloPedidoselec = (ArticuloPedido) adapterView.getItemAtPosition(posicion);
                        AlertDialog.Builder dialog1 = new AlertDialog.Builder(ArticulosActivity.this);
                        dialog1.setTitle("IMPORTANTE");
                        dialog1.setMessage("Quiere eliminar este Articulo?");
                        dialog1.setCancelable(false);
                        articuloPedidoselec.setId(String.valueOf(posicion));
                        dialog1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                ArticuloPedido a = new ArticuloPedido();
                                a.setId(articuloPedidoselec.getId());

                                myRef.child("Articulo").child(articuloPedidoselec.getId()).removeValue();
                                listaArticulos.remove(posicion);
                                adap.notifyDataSetChanged();
                            }
                        });
                        dialog1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        dialog1.show();
                        return false;

                    }
                });
        myRef.child("Articulo").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                if(snapshot.hasChildren()) {
                    listaArticulos.clear();
                    for (DataSnapshot dat: snapshot.getChildren()) {
                        ArticuloPedido art = dat.getValue(ArticuloPedido.class);
                        listaArticulos.add(art);
                        adap.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });


    }

}
