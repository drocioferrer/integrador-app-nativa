package com.example.integrador_app_nativa;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class ClienteAdapter extends RecyclerView.Adapter<ClienteAdapter.ViewHoldersClientes> {

    ArrayList<Cliente> listaClientes;

    public ClienteAdapter(ArrayList<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }

    @NotNull
    @Override
    public ViewHoldersClientes onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_clientes, null, false);
        return new ViewHoldersClientes(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHoldersClientes holder, int position) {
        holder.tv_id_cliente.setText("ID: " + listaClientes.get(position).id.toString());
        holder.tv_nombre_cliente.setText("Nombre: " + listaClientes.get(position).nombre);
        holder.tv_apellido_cliente.setText("Apellido: " + listaClientes.get(position).apellido);
        holder.tv_email_cliente.setText("Email: " + listaClientes.get(position).email);
        holder.tv_telefono_cliente.setText("Teléfono: " + listaClientes.get(position).telefono);
    }

    @Override
    public int getItemCount() {
        return listaClientes.size();
    }

    public class ViewHoldersClientes extends RecyclerView.ViewHolder {

        TextView tv_id_cliente, tv_nombre_cliente, tv_apellido_cliente, tv_email_cliente, tv_telefono_cliente;

        public ViewHoldersClientes(@NonNull @NotNull View itemView) {
            super(itemView);
            tv_id_cliente = (TextView) itemView.findViewById(R.id.tv_id_cliente);
            tv_nombre_cliente = (TextView) itemView.findViewById(R.id.tv_nombre_cliente);
            tv_apellido_cliente = (TextView) itemView.findViewById(R.id.tv_apellido_cliente);
            tv_email_cliente = (TextView) itemView.findViewById(R.id.tv_email_cliente);
            tv_telefono_cliente = (TextView) itemView.findViewById(R.id.tv_telefono_cliente);
        }
    }
}
