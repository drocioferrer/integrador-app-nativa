package com.example.integrador_app_nativa.Articulos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.integrador_app_nativa.Pedido.AgregarPedidoActivity;
import com.example.integrador_app_nativa.Pedido.PedidosActivity;
import com.example.integrador_app_nativa.R;
import com.example.integrador_app_nativa.modeloCliente.Articulo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AgregarArticuloActivity extends AppCompatActivity {

    EditText descripcionNA, precioNA;

    Button confirmarPed;

    ProgressBar progressBar;

    private static final int File = 1 ;
    DatabaseReference myRef;

    long maxId = 0;

    ArticuloPedido articuloPedido;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.uploadImageView)
    ImageView mUploadImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_articulo);

        descripcionNA = findViewById(R.id.textDescripcionNA);
        precioNA = findViewById(R.id.textPrecioNA);

        confirmarPed = findViewById(R.id.buttonPedidoConfirm);

        progressBar = findViewById(R.id.progressBar3);

        articuloPedido = new ArticuloPedido();

        ButterKnife.bind(this);

        FirebaseDatabase database = FirebaseDatabase.getInstance();

        myRef= database.getReference("Articulo");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                if (snapshot.exists())
                    maxId = snapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });

        mUploadImageView.setOnClickListener(v -> fileUpload());

    }

    public void fileUpload() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        progressBar.setVisibility(View.VISIBLE);
        Toast.makeText(AgregarArticuloActivity.this, "Espere unos segundos a que cargue la imagen y luego confirme", Toast.LENGTH_LONG).show();
        startActivityForResult(intent,File);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == File){

            if(resultCode == RESULT_OK){

                Uri FileUri = data.getData();

                StorageReference Folder = FirebaseStorage.getInstance().getReference();

                final StorageReference file_name = Folder.child("file"+FileUri.getLastPathSegment());


                file_name.putFile(FileUri).addOnSuccessListener(taskSnapshot -> file_name.getDownloadUrl().addOnSuccessListener(uri -> {

                    HashMap<String,String> hashMap = new HashMap<>();
                    hashMap.put("imagen", String.valueOf(uri));

                    confirmarPed.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //mUploadImageView.setOnClickListener(v -> fileUpload());
                            Long precio = Long.parseLong(precioNA.getText().toString());

                            articuloPedido.setDescripcion(descripcionNA.getText().toString());
                            articuloPedido.setPrecio(precio);
                            articuloPedido.setId(String.valueOf(maxId+1));
                            articuloPedido.setImagen(hashMap.put("imagen", String.valueOf(uri)));
                            myRef.child(String.valueOf(maxId + 1)).setValue(articuloPedido);

                            progressBar.setVisibility(View.VISIBLE);

                            descripcionNA.setText("");
                            precioNA.setText("");

                            Toast.makeText(AgregarArticuloActivity.this, "Articulo agregado correctamente", Toast.LENGTH_LONG).show();
                            finish();
                        }
                    });

                }));
            }

        }

    }
}