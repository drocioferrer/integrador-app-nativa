package com.example.integrador_app_nativa.Articulos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.example.integrador_app_nativa.R;
import com.example.integrador_app_nativa.modeloCliente.Articulo;

import java.util.ArrayList;

public class AdapterArticuloAdmin extends BaseAdapter {
    private Context contex;
    private ArrayList<ArticuloPedido> listItems;


    public AdapterArticuloAdmin(Context contex, ArrayList<ArticuloPedido> listItems) {
        this.contex = contex;
        this.listItems = listItems;
    }


    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ArticuloPedido item = (ArticuloPedido) getItem(position);
        convertView = LayoutInflater.from(contex).inflate(R.layout.item_artic, null);
        ImageView imgImagen = (ImageView) convertView.findViewById(R.id.imgArt);
        TextView tvDescripcionAdapter = (TextView) convertView.findViewById(R.id.txDescripcionAd);
        TextView tvPrecioAdapter = (TextView) convertView.findViewById(R.id.txPrecioAd);

        Glide.with(contex).load(item.imagen).into(imgImagen);
        tvDescripcionAdapter.setText(item.descripcion);
        tvPrecioAdapter.setText(item.precio.toString());
        return convertView;
    }
}
