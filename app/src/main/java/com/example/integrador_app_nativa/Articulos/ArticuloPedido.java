package com.example.integrador_app_nativa.Articulos;

public class ArticuloPedido {

    String id;
    String descripcion;
    Long precio;
    String imagen;

    public ArticuloPedido(String id, String descripcion, Long precio, String imagen) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
        this.imagen = imagen;
    }

    public ArticuloPedido() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getPrecio() {
        return precio;
    }

    public void setPrecio(Long precio) {
        this.precio = precio;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
