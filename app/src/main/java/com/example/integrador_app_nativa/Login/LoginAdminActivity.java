package com.example.integrador_app_nativa.Login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.integrador_app_nativa.MainActivity2;
import com.example.integrador_app_nativa.R;
import com.example.integrador_app_nativa.vistaCliente.MenuCliente;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import org.jetbrains.annotations.NotNull;


public class LoginAdminActivity extends AppCompatActivity {

    private EditText mEmail, mPassword;
    private Button mLoginBtn;
    private ProgressBar progressBar;
    private FirebaseAuth mAuth;

    private String txtEmail = "";
    private String txtPassword = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        mEmail = findViewById(R.id.textEmailLogin);
        mPassword = findViewById(R.id.textPasswordLogin);
        progressBar = findViewById(R.id.progressBar);
        mAuth = FirebaseAuth.getInstance();
        mLoginBtn = findViewById(R.id.buttonLogin);

        mLoginBtn.setOnClickListener(v -> {

            txtEmail = mEmail.getText().toString();
            txtPassword = mPassword.getText().toString();

            if(TextUtils.isEmpty(txtEmail) || TextUtils.isEmpty(txtPassword)) {
                Toast.makeText(LoginAdminActivity.this, "Email o contraseña vacios", Toast.LENGTH_SHORT).show();
            }

            if (txtPassword.length() < 6) {
                Toast.makeText(LoginAdminActivity.this, "Al menos 6 caracteres", Toast.LENGTH_SHORT).show();
            }

            progressBar.setVisibility(View.VISIBLE);

            //Acceso de usuario
            mAuth.signInWithEmailAndPassword(txtEmail, txtPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull @NotNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        if (txtEmail == "admin@gmail.com")
                        Toast.makeText(LoginAdminActivity.this, "Accediendo correctamente", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), MainActivity2.class));
                    }

                    else {
                        Toast.makeText(LoginAdminActivity.this, "Usuario no registrado", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
                    }
                }
            });

        });




    }
}