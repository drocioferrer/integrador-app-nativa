package com.example.integrador_app_nativa.Login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.integrador_app_nativa.MainActivity2;
import com.example.integrador_app_nativa.R;
import com.example.integrador_app_nativa.modeloCliente.Articulo;
import com.example.integrador_app_nativa.vistaCliente.MenuCliente;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.auth.User;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    private EditText nombre;
    private EditText apellido;
    private EditText telefono;
    private EditText email;
    private EditText password;
    private Button registrar2;
    private ProgressBar progressBar2;

    private String txtNombre = "";
    private String txtApellido = "";
    private String txtEmail = "";
    private String txtPassword = "";
    private String txtTelefono = "";

    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    DatabaseReference ref;

    ArrayList<Articulo> listaUsuario;

    long maxId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        nombre = findViewById(R.id.textNombre);
        apellido = findViewById(R.id.textApellido);
        email = findViewById(R.id.textEmail);
        password = findViewById(R.id.textPassword);
        telefono = findViewById(R.id.textPhone);
        registrar2 = findViewById(R.id.buttonRegister2);
        progressBar2 = findViewById(R.id.progressBar2);

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        ref = database.getReference("Usuario");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                if (snapshot.exists())
                    maxId = snapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });

        registrar2.setOnClickListener(view -> {
            txtNombre = nombre.getText().toString().trim();
            txtApellido = apellido.getText().toString().trim();
            txtEmail = email.getText().toString().trim();
            txtPassword = password.getText().toString().trim();
            txtTelefono = telefono.getText().toString().trim();

            if (TextUtils.isEmpty(txtNombre)) {
                Toast.makeText(RegisterActivity.this, "Nombre no puede ser vacio", Toast.LENGTH_SHORT).show();
            }

            else if (TextUtils.isEmpty(txtApellido)) {
                Toast.makeText(RegisterActivity.this, "Apellido no puede ser vacio", Toast.LENGTH_SHORT).show();
            }

            else if(TextUtils.isEmpty(txtEmail) || TextUtils.isEmpty(txtPassword)) {
                Toast.makeText(RegisterActivity.this, "Email o contraseña vacios", Toast.LENGTH_SHORT).show();
            }

            else if (txtPassword.length() < 6) {
                Toast.makeText(RegisterActivity.this, "Al menos 6 caracteres", Toast.LENGTH_SHORT).show();
            }

            else if (TextUtils.isEmpty(txtTelefono)) {
                Toast.makeText(RegisterActivity.this, "Telefono no puede ser vacio", Toast.LENGTH_SHORT).show();
            }

            else {

                registrarUsuario(txtEmail, txtPassword);

                agregarUsuario(txtNombre, txtApellido, txtEmail, txtTelefono);
            }

            progressBar2.setVisibility(View.VISIBLE);

        });
    }

    private void agregarUsuario(String txtNombre, String txtApellido, String txtEmail, String txtTelefono) {

        DatabaseReference usuarioRef = ref.child(String.valueOf(maxId + 1));

        usuarioRef.child("nombre").setValue(txtNombre);
        usuarioRef.child("apellido").setValue(txtApellido);
        usuarioRef.child("email").setValue(txtEmail);
        usuarioRef.child("telefono").setValue(txtTelefono);
        usuarioRef.child("id").setValue(maxId + 1).toString();
        usuarioRef.child("idRol").setValue(2);

    }


    private void registrarUsuario(String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(RegisterActivity.this, "Usuario creado correctamente", Toast.LENGTH_SHORT).show();
                    // INICIAR COMO CLIENTE
                    Intent intent = new Intent(RegisterActivity.this, MenuCliente.class);
                    intent.putExtra("usuario", "2");
                    startActivity(intent);
                    finish();

                }
                else {
                    Toast.makeText(RegisterActivity.this, "Registro fallido", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

}
