package com.example.integrador_app_nativa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.integrador_app_nativa.Login.MainActivity;
import com.example.integrador_app_nativa.Pedido.PedidosActivity;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity2 extends AppCompatActivity {

    private Button articulos;
    private Button clientes;
    private Button pedidos;
    private Button mLogOut;
    private Button MiRuta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = new Intent(MainActivity2.this, NuevoPedidoServicio.class);
        startService(intent);

        articulos = findViewById(R.id.buttonArticulos);
        clientes = findViewById(R.id.buttonClientes);
        pedidos = findViewById(R.id.buttonPedidos);
        mLogOut = findViewById(R.id.logOutButton);
        MiRuta = findViewById(R.id.MiRuta);

        articulos.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity2.this, ArticulosActivity.class));
            }
        });

        clientes.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity2.this, ClientesActivity.class));
            }
        });

        pedidos.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity2.this, PedidosActivity.class));
            }
        });

        MiRuta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity2.this, RutaActivity.class));
            }
        });

        mLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            }
        });

    }
}