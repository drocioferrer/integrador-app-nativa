package com.example.integrador_app_nativa.modeloCliente;

public class Usuario {

    public String id;
    public String nombre;
    public String email;

    public Usuario(){}

    public Usuario(String id, String nombre, String email){
        this.id = id;
        this.nombre = nombre;
        this.email = email;
    }
}
