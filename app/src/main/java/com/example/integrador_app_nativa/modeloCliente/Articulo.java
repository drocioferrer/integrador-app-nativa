package com.example.integrador_app_nativa.modeloCliente;

public class Articulo {
    public String id;
    public String descripcion;
    public Integer precio;
    public Integer cantidad;
    public Integer subtotal;
    public String imagen;

    public Articulo() {}

    public Articulo(String id, String descripcion, Integer precio, String imagen, Integer cantidad) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
        this.imagen = imagen;
        this.cantidad = cantidad;
    }

    public String toString() {
        return this.descripcion + " - $" + this.precio;
    }
}
