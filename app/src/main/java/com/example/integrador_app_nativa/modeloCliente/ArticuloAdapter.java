package com.example.integrador_app_nativa.modeloCliente;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.integrador_app_nativa.R;

import java.util.ArrayList;

public class ArticuloAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Articulo> listItems;
    private boolean mNotifyOnChange = true;

    public ArticuloAdapter(Context context, ArrayList<Articulo> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Articulo item = (Articulo) getItem(position);
        convertView = LayoutInflater.from(context).inflate(R.layout.item_articulo, null);
        ImageView imgImagen = (ImageView) convertView.findViewById(R.id.imgImagen);
        TextView tvDescripcionAdapter = (TextView) convertView.findViewById(R.id.tvDescripcionAdapter);
        TextView tvPrecioAdapter = (TextView) convertView.findViewById(R.id.tvPrecioAdapter);

        Glide.with(context).load(item.imagen).into(imgImagen);
        tvDescripcionAdapter.setText(item.descripcion);
        tvPrecioAdapter.setText("Precio: $" + item.precio.toString());

        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        mNotifyOnChange = true;
    }
}
