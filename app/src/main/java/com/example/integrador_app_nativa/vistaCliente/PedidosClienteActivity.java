package com.example.integrador_app_nativa.vistaCliente;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.integrador_app_nativa.Pedido.FetchDataPedido;
import com.example.integrador_app_nativa.Pedido.HelperAdapter;
import com.example.integrador_app_nativa.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class PedidosClienteActivity extends AppCompatActivity {

    List<FetchDataPedido> fetchDataPedidoList;
    RecyclerView rvPedidosCliente;
    HelperAdapter helperAdapter;

    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos_cliente);

        setTitle("Mis pedidos");

        String id = getIntent().getExtras().getString("usuario");

        rvPedidosCliente = findViewById(R.id.rvPedidosCliente);

        rvPedidosCliente.setLayoutManager(new LinearLayoutManager(this));

        fetchDataPedidoList = new ArrayList<>();

        databaseReference = FirebaseDatabase.getInstance().getReference("Pedido");

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot ds:snapshot.getChildren()){
                    FetchDataPedido dataPedido = ds.getValue(FetchDataPedido.class);
                    if (dataPedido.getCliente().equals(id)){
                        fetchDataPedidoList.add(dataPedido);
                    }
                }

                helperAdapter = new HelperAdapter(fetchDataPedidoList);
                rvPedidosCliente.setAdapter(helperAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}