package com.example.integrador_app_nativa.vistaCliente;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.integrador_app_nativa.Login.LoginActivity;
import com.example.integrador_app_nativa.modeloCliente.CarritoAdapter;
import com.example.integrador_app_nativa.Database.DatabaseHelper;
import com.example.integrador_app_nativa.R;
import com.example.integrador_app_nativa.modeloCliente.Articulo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.annotations.NotNull;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;

public class CarritoActivity extends AppCompatActivity {

    ListView lvCarrito;
    TextView tvTotal;
    Button btnConfirmarPedido;
    TextView tvCarritoVacio;
    TextView tvUbicacion;
    TextView tvEnvio;
    Button btnUbicacion;

    private CarritoAdapter adaptador;

    String idUsuario;

    FirebaseDatabase database;
    DatabaseReference myRef;

    SQLiteDatabase bd;
    DatabaseHelper helper = new DatabaseHelper(this);

    ArrayList<Articulo> listaArticulos;

    long total;
    long maxId = 0;
    String ubicacion = null;

    private LocationManager locationManager = null;
    private MyLocationListener locationListener = null;

    private Boolean flag = false;

    int year;
    int month;
    int dayOfMonth;
    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente_carrito);

        lvCarrito = findViewById(R.id.lvCarrito);
        tvTotal = findViewById(R.id.tvTotal);
        tvCarritoVacio = findViewById(R.id.tvCarritoVacio);
        tvUbicacion = findViewById(R.id.tvUbicacion);
        tvEnvio = findViewById(R.id.tvEnvio);
        btnConfirmarPedido = findViewById(R.id.btnConfirmarPedido);
        btnUbicacion = findViewById(R.id.btnUbicacion);
        setTitle("Carrito de Compras");

        // OBTENER EL LOCATION MANAGER
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        idUsuario = getIntent().getExtras().getString("usuario");

        consultarListaArticulos();

        adaptador = new CarritoAdapter(this, listaArticulos);
        lvCarrito.setAdapter(adaptador);


        if(listaArticulos.size()==0){
            btnConfirmarPedido.setVisibility(View.INVISIBLE);
            tvTotal.setVisibility(View.INVISIBLE);
            tvEnvio.setVisibility(View.INVISIBLE);
            tvUbicacion.setVisibility(View.INVISIBLE);
            btnUbicacion.setVisibility(View.INVISIBLE);
        } else {
            tvCarritoVacio.setVisibility(View.INVISIBLE);
        }

        if(ubicacion == null){
            btnConfirmarPedido.setVisibility(View.INVISIBLE);
        }

        lvCarrito.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(CarritoActivity.this, DetalleItemCarritoActivity.class);
                intent.putExtra("articulo", listaArticulos.get(position).id);
                intent.putExtra("usuario", idUsuario);
                startActivity(intent);
            }
        });

        mostrarTotal();

        myRef = FirebaseDatabase.getInstance().getReference("Pedido");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                if (snapshot.exists())
                    maxId = snapshot.getChildrenCount();
            }
            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {
            }
        });

        btnUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // VERIFICAR ESTADO DEL GPS
                flag = displayGpsStatus();

                if (flag) {
                    tvUbicacion.setText("Espere..");

                    // INSTANCIAR EL LISTENER
                    locationListener = new MyLocationListener();

                    // RECUPERAR LAS COORDENADAS
                    if (ActivityCompat.checkSelfPermission(CarritoActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(CarritoActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER, 3000, 10, (LocationListener) locationListener);
                } else {
                    // GPS DESABILITADO
                    showInfoAlert();
                }
            }
        });

        btnConfirmarPedido.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                // AGREGAR PEDIDO A BASE DE DATOS FIREBASE

                DatabaseReference pedidoRef = myRef.child(String.valueOf(maxId + 1));

                for (int i = 0; i < listaArticulos.size(); i++){
                    String id= listaArticulos.get(i).id;
                    String cantidad = String.valueOf(listaArticulos.get(i).cantidad);
                    String precio = String.valueOf(listaArticulos.get(i).precio);
                    pedidoRef.child("articulos").child(id).child("id").setValue(id);
                    pedidoRef.child("articulos").child(id).child("cantidad").setValue(cantidad);
                    pedidoRef.child("articulos").child(id).child("precio").setValue(precio);
                    pedidoRef.child("articulo" + id).setValue(cantidad);
                }

                pedidoRef.child("cliente").setValue(idUsuario);
                pedidoRef.child("ubicacion").setValue(ubicacion);
                pedidoRef.child("estado").setValue("Pendiente");
                pedidoRef.child("monto").setValue(total);
                pedidoRef.child("numero").setValue(maxId + 1);

                calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                pedidoRef.child("fecha").setValue(dayOfMonth+"/"+(month+1)+"/"+year);

                // LIMPIAR CARRITO
                bd = helper.open();
                bd.delete("carrito", null, null);
                helper.close();

                AlertConfirmarCompra();
            }
        });
    }

    // CHECKEAR SI EL GPS ESTA HABILITADO
    private Boolean displayGpsStatus() {
        ContentResolver contentResolver = getBaseContext().getContentResolver();
        boolean gpsStatus = Settings.Secure.isLocationProviderEnabled(
                contentResolver, LocationManager.NETWORK_PROVIDER);
        if (gpsStatus) {
            return true;

        } else {
            return false;
        }
    }

    private void mostrarTotal() {
        SQLiteDatabase db =  helper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT SUM(subtotal) FROM carrito", null);
        if(cursor!=null) {
            cursor.moveToFirst();
            if(cursor.getString(0) == null){
                tvTotal.setText("Total: $0");
            } else {
                total = cursor.getLong(0);
                tvTotal.setText("Total: $" + total);
            }
        }
    }

    private void consultarListaArticulos() {
        SQLiteDatabase db =  helper.getReadableDatabase();
        Articulo articulo = null;
        listaArticulos = new ArrayList<Articulo>();
        Cursor cursor = db.rawQuery("SELECT * FROM carrito", null);

        while(cursor.moveToNext()){
            articulo = new Articulo();
            articulo.id = cursor.getString(1);
            articulo.descripcion = cursor.getString(2);
            articulo.precio = cursor.getInt(3);
            articulo.cantidad = cursor.getInt(4);
            articulo.subtotal = cursor.getInt(5);
            articulo.imagen = cursor.getString(6);

            listaArticulos.add(articulo);
        }
    }

    private void showInfoAlert() {
        new AlertDialog.Builder(this)
                .setTitle("Activar ubicación")
                .setMessage("¿Querés activar la ubicacion de tu dispositivo?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancelar", null)
                .show();
    }

    private void AlertConfirmarCompra() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        dialog.setView(inflater.inflate(R.layout.dialog, null))
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(CarritoActivity.this, MenuCliente.class);
                        intent.putExtra("usuario", idUsuario);
                        startActivity(intent);
                    }
                });
        AlertDialog alert = dialog.create();
        alert.show();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CarritoActivity.this, ArticulosClienteActivity.class);
        intent.putExtra("usuario", idUsuario);
        startActivity(intent);
    }

    //LISTENER QUE OBTIENE LAS COORDENADAS
    public class MyLocationListener implements LocationListener {

        private double longitude, latitude;

        @Override
        public void onLocationChanged(Location loc) {

            this.longitude = loc.getLongitude();
            this.latitude = loc.getLatitude();

            // MOSTRAR COORDENADAS
            ubicacion = this.latitude + "," + this.longitude;
            tvUbicacion.setText(ubicacion);

            if(ubicacion != null){
                btnUbicacion.setVisibility(View.INVISIBLE);
                Toast.makeText(CarritoActivity.this, "Se ha cargado tu ubicación", Toast.LENGTH_SHORT).show();
                btnConfirmarPedido.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }
}