package com.example.integrador_app_nativa.vistaCliente;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.integrador_app_nativa.Login.MainActivity;
import com.example.integrador_app_nativa.R;
import com.google.firebase.auth.FirebaseAuth;

public class MenuCliente extends AppCompatActivity {

    private Button btnArticulos;
    private Button btnPedidos;
    private Button btnLogout;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente_menu);

        String idUsuario = getIntent().getExtras().getString("usuario");

        btnArticulos = findViewById(R.id.btnArticulos);
        btnPedidos = findViewById(R.id.btnPedidos);
        btnLogout = findViewById(R.id.btnLogout);
        mAuth = FirebaseAuth.getInstance();

        btnArticulos.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuCliente.this, ArticulosClienteActivity.class);
                intent.putExtra("usuario", idUsuario);
                startActivity(intent);
            }
        });

        btnPedidos.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuCliente.this, PedidosClienteActivity.class);
                intent.putExtra("usuario", idUsuario);
                startActivity(intent);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            }
        });

    }
}