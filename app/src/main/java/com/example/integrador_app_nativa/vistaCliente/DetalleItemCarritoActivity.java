package com.example.integrador_app_nativa.vistaCliente;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.integrador_app_nativa.Database.DatabaseHelper;
import com.example.integrador_app_nativa.R;

public class DetalleItemCarritoActivity extends AppCompatActivity {

    SQLiteDatabase bd;
    DatabaseHelper helper = new DatabaseHelper(this);
    String idArticulo;
    String idUsuario;
    TextView tvNombreItem;
    TextView tvItemPrecio;
    ImageView btnItemMas;
    TextView tvItemCantidad;
    ImageView btnItemMenos;
    ImageView btnItemEliminar;
    TextView tvItemSubtotal;
    Integer cantidadActual;
    Integer subtotalActual;
    Integer precio;
    ImageView ivImagenItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_item_carrito);

        idArticulo = getIntent().getExtras().getString("articulo");
        idUsuario = getIntent().getExtras().getString("usuario");

        tvNombreItem = findViewById(R.id.tvNombreItem);
        tvItemPrecio = findViewById(R.id.tvItemPrecio);
        btnItemMas = findViewById(R.id.btnItemMas);
        tvItemCantidad = findViewById(R.id.tvItemCantidad);
        btnItemMenos = findViewById(R.id.btnItemMenos);
        btnItemEliminar = findViewById(R.id.btnItemEliminar);
        tvItemSubtotal = findViewById(R.id.tvItemSubtotal);
        ivImagenItem = findViewById(R.id.ivImagenItem);

        mostrarItem();

        btnItemMas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cantidadActual++;
                subtotalActual = cantidadActual * precio;
                setText();

                bd = helper.open();
                ContentValues registro = new ContentValues();
                registro.put("cantidad", cantidadActual);
                registro.put("subtotal", subtotalActual);

                int cant = bd.update("carrito", registro, "idArticulo='"+idArticulo+"'", null);
                if (cant > 0) {
                    Toast.makeText(DetalleItemCarritoActivity.this, "Se modificó la cantidad",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(DetalleItemCarritoActivity.this, "No existe el articulo",
                            Toast.LENGTH_SHORT).show();
                }
                helper.close();
                Intent intent = new Intent(DetalleItemCarritoActivity.this, CarritoActivity.class);
                intent.putExtra("usuario", idUsuario);
                startActivity(intent);
            }
        });

        btnItemMenos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cantidadActual>1){
                    cantidadActual--;
                }
                setText();
                subtotalActual = cantidadActual * precio;
                setText();

                bd = helper.open();
                ContentValues registro = new ContentValues();
                registro.put("cantidad", cantidadActual);
                registro.put("subtotal", subtotalActual);

                int cant = bd.update("carrito", registro, "idArticulo='"+idArticulo+"'", null);
                if (cant > 0) {
                    Toast.makeText(DetalleItemCarritoActivity.this, "Se modificó la cantidad",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(DetalleItemCarritoActivity.this, "No existe el articulo",
                            Toast.LENGTH_SHORT).show();
                }
                helper.close();
                Intent intent = new Intent(DetalleItemCarritoActivity.this, CarritoActivity.class);
                intent.putExtra("usuario", idUsuario);
                startActivity(intent);
            }
        });

        btnItemEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DetalleItemCarritoActivity.this);
                builder.setIcon(R.mipmap.icono).
                        setTitle("Eliminar").
                        setMessage("¿Desea eliminar el artículo seleccionado?").
                        setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                bd = helper.open();
                                int cant = bd.delete("carrito", "idArticulo='"+idArticulo+"'", null);
                                if (cant > 0) {
                                    Toast.makeText(DetalleItemCarritoActivity.this, "Se borró el articulo", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(DetalleItemCarritoActivity.this, "No existe el articulo", Toast.LENGTH_SHORT).show();
                                }
                                helper.close();
                                Intent intent = new Intent(DetalleItemCarritoActivity.this, CarritoActivity.class);
                                intent.putExtra("usuario", idUsuario);
                                startActivity(intent);                            }
                        }).
                        setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
    }

    private void setText() {
        tvItemCantidad.setText(cantidadActual+"");
    }


    private void mostrarItem() {
        String buscar = idArticulo;
        String consulta = "SELECT idArticulo, descripcion, precio, cantidad, subtotal, imagen FROM carrito where " +
                "(idArticulo like '" + buscar + "%')";
        bd = helper.open();
        Cursor fila = bd.rawQuery(consulta, null);
        if (fila.moveToFirst()) {
            bd = helper.open();
            tvNombreItem.setText(fila.getString(1));
            tvItemPrecio.setText("$" + fila.getString(2));
            tvItemCantidad.setText(fila.getString(3));
            tvItemSubtotal.setText("Subtotal: $" + fila.getString(4));
            precio= fila.getInt(2);
            cantidadActual= fila.getInt(3);
            subtotalActual= fila.getInt(4);
            Glide.with(DetalleItemCarritoActivity.this).load(fila.getString(5)).into(ivImagenItem);
            helper.close();
        }
    }
}