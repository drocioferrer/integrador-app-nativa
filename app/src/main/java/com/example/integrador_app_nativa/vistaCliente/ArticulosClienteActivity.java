package com.example.integrador_app_nativa.vistaCliente;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.integrador_app_nativa.R;
import com.example.integrador_app_nativa.modeloCliente.Articulo;
import com.example.integrador_app_nativa.modeloCliente.ArticuloAdapter;
import com.example.integrador_app_nativa.modeloCliente.Usuario;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ArticulosClienteActivity extends AppCompatActivity {

    Button btnCarrito;
    ListView lvArticulos;
    Usuario usuario;

    FirebaseDatabase database;
    DatabaseReference myRef;

    ArrayList<Articulo> listaArticulos;

    private ArticuloAdapter adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente_articulos);

        String id = getIntent().getExtras().getString("usuario");
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        btnCarrito = findViewById(R.id.btnCarrito);
        lvArticulos = findViewById(R.id.lvArticulos);

        listaArticulos = new ArrayList<>();

        adaptador = new ArticuloAdapter(this, listaArticulos);
        lvArticulos.setAdapter(adaptador);

        lvArticulos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ArticulosClienteActivity.this, DetalleArticuloActivity.class);
                intent.putExtra("articulo", listaArticulos.get(position).id);
                intent.putExtra("usuario", usuario.id);
                startActivity(intent);
            }
        });

        myRef.child("Articulo").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChildren()) {
                    listaArticulos.clear();
                    for (DataSnapshot dato: dataSnapshot.getChildren()) {
                        Articulo articulo = dato.getValue(Articulo.class);
                        listaArticulos.add(articulo);
                        adaptador.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        myRef.child("Usuario").child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                usuario = dataSnapshot.getValue(Usuario.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

        btnCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ArticulosClienteActivity.this, CarritoActivity.class);
                intent.putExtra("usuario", usuario.id);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ArticulosClienteActivity.this, MenuCliente.class);
        intent.putExtra("usuario", usuario.id);
        startActivity(intent);
    }
}