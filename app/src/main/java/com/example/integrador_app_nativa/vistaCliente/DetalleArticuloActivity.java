  package com.example.integrador_app_nativa.vistaCliente;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.integrador_app_nativa.Database.DatabaseHelper;
import com.example.integrador_app_nativa.R;
import com.example.integrador_app_nativa.modeloCliente.Articulo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DetalleArticuloActivity extends AppCompatActivity {

    TextView tvDescripcion;
    TextView tvPrecio;
    Button btnAgregar;
    Articulo articulo;
    String idArticulo;
    String idUsuario;
    TextView txtCantidad;

    Integer cantidad = 1;

    FirebaseDatabase database;
    DatabaseReference myRef;

    SQLiteDatabase bd;
    DatabaseHelper helper = new DatabaseHelper(this);

    ImageView btnMenos;
    ImageView btnMas;
    ImageView ivImagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_articulo);

        tvDescripcion = findViewById(R.id.tvDescripcion);
        tvPrecio = findViewById(R.id.tvPrecio);
        btnAgregar = findViewById(R.id.btnAgregar);

        btnMenos = findViewById(R.id.btnItemMenos);
        btnMas = findViewById(R.id.btnItemMas);
        txtCantidad = findViewById(R.id.tvItemCantidad);
        ivImagen = findViewById(R.id.ivImagen);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        idArticulo = getIntent().getExtras().getString("articulo");
        idUsuario = getIntent().getExtras().getString("usuario");

        myRef.child("Articulo").child(idArticulo).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                articulo = dataSnapshot.getValue(Articulo.class);
                tvDescripcion.setText(articulo.descripcion);
                tvPrecio.setText("Precio: $" + articulo.precio);
                Glide.with(DetalleArticuloActivity.this).load(articulo.imagen).into(ivImagen);
                setTitle("Detalle: " + articulo.descripcion);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

        btnMas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cantidad++;
                setText();
            }
        });
        btnMenos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cantidad>1){
                    cantidad--;
                }
                setText();
            }
        });

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String buscar = articulo.id;
                String consulta = "SELECT idArticulo, cantidad, subtotal FROM carrito where " +
                        "(idArticulo like '" + buscar + "%')";
                bd = helper.open();
                Cursor fila = bd.rawQuery(consulta, null);
                Integer cantidadanterior;
                Integer subtotalanterior;
                if (fila.moveToFirst()) {
                    bd = helper.open();
                    cantidadanterior = fila.getInt(1);
                    subtotalanterior = fila.getInt(2);

                    ContentValues registro = new ContentValues();
                    registro.put("cantidad", cantidadanterior + cantidad);
                    registro.put("subtotal", subtotalanterior + (articulo.precio * cantidad));

                    int cant = bd.update("carrito", registro, "idArticulo='"+articulo.id+"'", null);
                    if (cant > 0) {
                        Toast.makeText(DetalleArticuloActivity.this, "Se modificó el articulo",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(DetalleArticuloActivity.this, "No existe el articulo",
                                Toast.LENGTH_SHORT).show();
                    }
                    helper.close();
                } else {
                    ContentValues registro = new ContentValues();
                    registro.put("idArticulo", articulo.id.toString());
                    registro.put("descripcion", articulo.descripcion.toString());
                    registro.put("precio", articulo.precio);
                    registro.put("cantidad", cantidad);
                    registro.put("subtotal", articulo.precio * cantidad);
                    registro.put("imagen", articulo.imagen);
                    bd = helper.open();
                    bd.insert("carrito", null, registro);
                    helper.close();
                    Toast.makeText(DetalleArticuloActivity.this, "Se ha guardado el articulo en el carrito",
                            Toast.LENGTH_SHORT).show();
                }
                helper.close();
                finish();
            }
        });
    }

    public void setText(){
        txtCantidad.setText(cantidad+"");
    }
}