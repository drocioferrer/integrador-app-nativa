package com.example.integrador_app_nativa;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.Calendar;

public class RutaActivity extends AppCompatActivity {

    Button btnfecha;
    TextView tvFecha;
    DatePickerDialog datePickerDialog;
    int year;
    int month;
    int dayOfMonth;
    Calendar calendar;
    String fecha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ruta);

        setTitle("Mi ruta");

        btnfecha = findViewById(R.id.btnfecha);
        tvFecha = findViewById(R.id.tvFecha);

        btnfecha.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(RutaActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                tvFecha.setText("Fecha: " + day + "/" + (month  +1) + "/" + year);
                                fecha = day + "/" + (month  +1) + "/" + year;
                                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                                transaction.replace(R.id.contenedorFragments, new MapsFragment(fecha));
                                transaction.commit();
                            }
                        }, year, month, dayOfMonth);
                datePickerDialog.show();
            }
        });
    }
}