package com.example.integrador_app_nativa;

public class Cliente {

    public String id;
    public String nombre;
    public String apellido;
    public String email;
    public String telefono;
    public String idRol;

    public Cliente () {}

    public Cliente (String id, String nombre, String apellido, String email, String telefono, String idRol) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.telefono = telefono;
        this.idRol = idRol;
    }
}
