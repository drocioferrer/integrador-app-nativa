package com.example.integrador_app_nativa.Pedido;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.integrador_app_nativa.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class HelperAdapter2 extends RecyclerView.Adapter {

    List<String> fetchData;
    ArrayList<String> subitemListFinal=new ArrayList<>();

    public HelperAdapter2(List<String> fetchData) {

        this.fetchData = fetchData;
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout2, parent, false);
        ViewHolderClass viewHolderClass = new ViewHolderClass(view);

        return viewHolderClass;
    }

    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder holder, int position) {

        ViewHolderClass viewHolderClass = (ViewHolderClass)holder;
        final String fetchDatalist = fetchData.get(position);
        viewHolderClass.numero.setText(fetchDatalist);

    }

    @Override
    public int getItemCount() {
        return fetchData.size();
    }

    public class ViewHolderClass extends RecyclerView.ViewHolder{

        TextView numero;

        public ViewHolderClass(@NonNull View itemView) {
            super(itemView);
            numero = itemView.findViewById(R.id.numero);

            //Typeface face = Typeface.defaultFromStyle(Typeface.BOLD);
            //numero.setTypeface(face);
        }
    }
}
