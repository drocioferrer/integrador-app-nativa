package com.example.integrador_app_nativa.Pedido;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.integrador_app_nativa.R;
import com.example.integrador_app_nativa.modeloCliente.Articulo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.core.Context;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class HelperAdapter extends RecyclerView.Adapter {

    List<FetchDataPedido> fetchData;
    Context context;

    public HelperAdapter(List<FetchDataPedido> fetchData) {
        this.fetchData = fetchData;

    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout,parent, false);
        ViewHolderClass viewHolderClass = new ViewHolderClass(view);

        return viewHolderClass;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder holder, final int position) {

        ViewHolderClass viewHolderClass = (ViewHolderClass)holder;
        final FetchDataPedido fetchDatalist = fetchData.get(position);
        viewHolderClass.numero.setText("N°Pedido: " + fetchDatalist.getNumero().toString());
        viewHolderClass.cliente.setText("Cliente: " + fetchDatalist.getCliente());
        viewHolderClass.ubicacion.setText("Ubicacion: " + fetchDatalist.getUbicacion());
        viewHolderClass.monto.setText("Monto: $" + fetchDatalist.getMonto().toString());
        viewHolderClass.estado.setText("Estado actual: " + fetchDatalist.getEstado());
        viewHolderClass.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), DetallePedido.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("key",fetchDatalist);
                intent.putExtras(bundle);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return fetchData.size();
    }

    public class ViewHolderClass extends RecyclerView.ViewHolder{

        TextView cliente, numero, ubicacion, monto, estado;

        public ViewHolderClass(@NotNull View itemView) {
            super(itemView);
            numero = itemView.findViewById(R.id.numero);
            cliente = itemView.findViewById(R.id.cliente);
            ubicacion = itemView.findViewById(R.id.domicilio);
            monto = itemView.findViewById(R.id.monto);
            estado = itemView.findViewById(R.id.estado);
        }
    }
}
