package com.example.integrador_app_nativa.Pedido;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.integrador_app_nativa.Articulos.AgregarArticuloActivity;
import com.example.integrador_app_nativa.ArticulosActivity;
import com.example.integrador_app_nativa.MainActivity2;
import com.example.integrador_app_nativa.R;

import com.example.integrador_app_nativa.modeloCliente.Articulo;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class DetallePedido extends AppCompatActivity {

    HelperAdapter2 helperAdapter2;
    RecyclerView recyclerViewDetalle;

    RadioGroup radioGroup;
    RadioButton radioButtonPendiente, radioButtonConfirmar, radioButtonEntregado;

    Button buttonAceptarPedido;

    String articulo1, articulo2, articulo3, articulo4;

    DatabaseReference databaseReference;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalle_pedido);
        recyclerViewDetalle = findViewById(R.id.recyclerViewSecond);

        radioGroup = findViewById(R.id.radioGroupDP);

        radioButtonPendiente = findViewById(R.id.radioButtonPendiente);
        String textPendiente = radioButtonPendiente.getText().toString();

        radioButtonConfirmar = findViewById(R.id.radioButtonConfirmar);
        String textConfirmar = radioButtonConfirmar.getText().toString();

        radioButtonEntregado = findViewById(R.id.radioButtonEntregado);
        String textEntregado = radioButtonEntregado.getText().toString();

        buttonAceptarPedido = findViewById(R.id.buttonAceptarPedido);

        databaseReference = FirebaseDatabase.getInstance().getReference("Pedido");

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        FetchDataPedido fetchDatalist = (FetchDataPedido) bundle.getSerializable("key");
        recyclerViewDetalle.setLayoutManager(new LinearLayoutManager(this));

        articulo1 = fetchDatalist.getArticulo1();
        articulo2 = fetchDatalist.getArticulo2();
        articulo3 = fetchDatalist.getArticulo3();
        articulo4 = fetchDatalist.getArticulo4();

        ArrayList<String> arrayList = new ArrayList<>();

        arrayList.add("Pedido N°: " + fetchDatalist.getNumero().toString());
        arrayList.add("Cliente: " + fetchDatalist.getCliente());

        arrayList.add("Cant  " + "              Articulo");


        if (articulo1 != null) {
            arrayList.add("   " + articulo1 + "      x   " + "  Bidón de agua 12 Lts");
        }

        if (articulo2 != null) {
            arrayList.add("   " + articulo2 + "      x   " + "  Bidón de agua 20 Lts");
        }

        if (articulo3 != null) {
            arrayList.add("   " + articulo3 + "      x   " + "  Dispenser de agua fría y caliente");
        }

        if (articulo4 != null) {
            arrayList.add("   " + articulo4 + "      x   " + "  Sifón de soda 2 Lts ");
        }
        arrayList.add("");
        arrayList.add("Total: $ " + fetchDatalist.getMonto().toString());
        arrayList.add("");
        arrayList.add("Estado actual: " + fetchDatalist.getEstado());
        arrayList.add("Ubicacion: " + fetchDatalist.getUbicacion());

        helperAdapter2 = new HelperAdapter2(arrayList);
        recyclerViewDetalle.setAdapter(helperAdapter2);

        buttonAceptarPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radioButtonPendiente.isChecked()) {
                    databaseReference.child(fetchDatalist.getNumero().toString()).child("estado").setValue(textPendiente);
                    Toast.makeText(DetallePedido.this, "Pedido pasado a pendiente", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(DetallePedido.this, PedidosActivity.class));
                    finish();
                }

                if (radioButtonConfirmar.isChecked()) {
                    databaseReference.child(fetchDatalist.getNumero().toString()).child("estado").setValue(textConfirmar);
                    Toast.makeText(DetallePedido.this, "Pedido confirmado", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(DetallePedido.this, PedidosActivity.class));
                    finish();
                }

                if (radioButtonEntregado.isChecked()) {
                    databaseReference.child(fetchDatalist.getNumero().toString()).child("estado").setValue(textEntregado);
                    Toast.makeText(DetallePedido.this, "Pedido entregado", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(DetallePedido.this, PedidosActivity.class));
                    finish();
                }

                else {
                    finish();
                }

            }
        });

    }

}

