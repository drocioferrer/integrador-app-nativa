package com.example.integrador_app_nativa.Pedido;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.integrador_app_nativa.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

public class AgregarPedidoActivity extends AppCompatActivity {

    EditText clienteNP, articuloNP, cantidadNP, domicilioNP, estadoNP, montoNP,estadoDeudaNP, notasNP;

    Button buttonNP;

    long maxId = 0;

    FetchDataPedido pedido;

    DatabaseReference myRefDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_pedido);

        clienteNP = findViewById(R.id.textClienteNP);
        articuloNP = findViewById(R.id.textArticuloNP);
        cantidadNP = findViewById(R.id.textCantidadNP);
        domicilioNP = findViewById(R.id.textDomicilioNP);
        estadoNP = findViewById(R.id.textEstadoNP);
        montoNP = findViewById(R.id.textMontoNP);
        estadoDeudaNP = findViewById(R.id.textEstadoDeudaNP);
        notasNP = findViewById(R.id.textNotasNP);

        buttonNP = findViewById(R.id.buttonNP);

        pedido = new FetchDataPedido();

        myRefDB = FirebaseDatabase.getInstance().getReference("Pedido");
        myRefDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                if (snapshot.exists())
                    maxId = snapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });

        buttonNP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Long cantidad = Long.parseLong(cantidadNP.getText().toString());
                Long monto = Long.parseLong(montoNP.getText().toString());
                Long idarticulo = Long.parseLong(articuloNP.getText().toString());

                pedido.setCliente(clienteNP.getText().toString());
                pedido.setUbicacion(domicilioNP.getText().toString());
                pedido.setEstado(estadoNP.getText().toString());
                pedido.setEstadoDeudaPedido(estadoDeudaNP.getText().toString());
                pedido.setNotas(notasNP.getText().toString());
                //pedido.setCantidad(cantidad);
                //pedido.setIdArticulo(idarticulo);
                pedido.setMonto(monto);
                pedido.setNumero(maxId+1);
                myRefDB.child(String.valueOf(maxId + 1)).setValue(pedido);

                Toast.makeText(AgregarPedidoActivity.this, "Pedido agregado correctamente", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(AgregarPedidoActivity.this, PedidosActivity.class));
                finish();
            }
        });
    }
}
