package com.example.integrador_app_nativa.Pedido;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.integrador_app_nativa.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class PedidosActivity extends AppCompatActivity {

    Button agregarPedido;
    List<FetchDataPedido> fetchDataPedidoList;

    RecyclerView recyclerView;
    HelperAdapter helperAdapter;

    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos);

        agregarPedido = findViewById(R.id.buttonAgregarPedido);
        recyclerView = findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        fetchDataPedidoList = new ArrayList<>();

        databaseReference = FirebaseDatabase.getInstance().getReference("Pedido");

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot ds:snapshot.getChildren()){
                    FetchDataPedido dataPedido = ds.getValue(FetchDataPedido.class);
                    fetchDataPedidoList.add(dataPedido);
                }

                helperAdapter = new HelperAdapter(fetchDataPedidoList);
                recyclerView.setAdapter(helperAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        agregarPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PedidosActivity.this, AgregarPedidoActivity.class));
            }
        });
    }
}