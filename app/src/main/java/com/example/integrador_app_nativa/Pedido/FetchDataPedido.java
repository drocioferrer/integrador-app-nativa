package com.example.integrador_app_nativa.Pedido;

import android.graphics.Typeface;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FetchDataPedido implements Serializable {

    String articulo1;
    String articulo2;
    String articulo3;
    String articulo4;
    String cliente;
    Long numero;
    String ubicacion;
    Long monto;
    String estado;
    String cantidad;
    String estadoDeudaPedido;
    String notas;
    Long idUsuario;

    public FetchDataPedido(String articulo1, String articulo2, String articulo3, String articulo4, String cliente, Long numero, String Ubicacion, Long monto, String estado, String cantidad, String estadoDeudaPedido, String notas, Long idUsuario) {
        this.articulo1 = articulo1;
        this.articulo2 = articulo2;
        this.articulo3 = articulo3;
        this.articulo4 = articulo4;
        this.cliente = cliente;
        this.numero = numero;
        this.ubicacion = Ubicacion;
        this.monto = monto;
        this.estado = estado;
        this.cantidad = cantidad;
        this.estadoDeudaPedido = estadoDeudaPedido;
        this.notas = notas;
        this.idUsuario = idUsuario;
    }

    public FetchDataPedido(){}

    public String getArticulo1() {
        return articulo1;
    }

    public void setArticulo1(String articulo1) {
        this.articulo1 = articulo1;
    }

    public String getArticulo2() {
        return articulo2;
    }

    public void setArticulo2(String articulo2) {
        this.articulo2 = articulo2;
    }

    public String getArticulo3() {
        return articulo3;
    }

    public void setArticulo3(String articulo3) {
        this.articulo3 = articulo3;
    }

    public String getArticulo4() {
        return articulo4;
    }

    public void setArticulo4(String articulo4) {
        this.articulo4 = articulo4;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public String getUbicacion() {
        return this.ubicacion;
    }

    public void setUbicacion(String Ubicacion) {
        ubicacion = Ubicacion;
    }

    public Long getMonto() {
        return monto;
    }

    public void setMonto(Long Monto) {
        monto = Monto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String Estado) {
        estado = Estado;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String Cantidad) {
        cantidad = Cantidad;
    }

    public String getEstadoDeudaPedido() {
        return estadoDeudaPedido;
    }

    public void setEstadoDeudaPedido(String EstadoDeudaPedido) {
        estadoDeudaPedido = EstadoDeudaPedido;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String Notas) {
        notas = Notas;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

}
