package com.example.integrador_app_nativa;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.example.integrador_app_nativa.modeloCliente.Articulo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ClientesActivity extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference myRef;

    ArrayList<Cliente> listaClientes;
    RecyclerView recyclerClientes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientes);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        listaClientes = new ArrayList<>();
        recyclerClientes = findViewById(R.id.recyclerClientes);
        recyclerClientes.setLayoutManager(new LinearLayoutManager(this));

        ClienteAdapter adapter = new ClienteAdapter(listaClientes);
        recyclerClientes.setAdapter(adapter);

        myRef.child("Usuario").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChildren()) {
                    listaClientes.clear();
                    for (DataSnapshot dato: dataSnapshot.getChildren()) {
                        Cliente cliente = dato.getValue(Cliente.class);
                        if (cliente.idRol.equals("2")) {
                            listaClientes.add(cliente);
                            adapter.notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        ;

    }


}